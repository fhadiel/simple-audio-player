const URL = require('url')
const Path = require('path')
const Find = require('find')
const RendererEvents = require('./modules/renderer-events.js')
const Ne = require('./modules/nedb.js')
const {
  app,
  BrowserWindow,
  ipcMain,
  Menu,
} = Electron = require('electron')

app.setName('FHPlayer')

const App = {
  MainWindow: null,
  MainURL : URL.format({
    protocol: 'http',
    hostname: 'localhost',
    port: 8080
  }),
  FilePath: URL.format({
    protocol: 'file',
    slashes: true,
    pathname: Path.join(__dirname,'/dist/index.html')
  })
}

SetAppMenu(require('./modules/menu.js'))
const LaunchWindow = () => {
  const Display = Electron.screen.getAllDisplays()
  const SpawnPos = {x:0,y:30}
  App.MainWindow = new BrowserWindow({
    width:600,
    height:250,
    ...SpawnPos,
  })
  App.MainWindow.webContents.openDevTools()
  App.MainWindow.loadURL(App.MainURL)
  // 'main page is ready!'
}
app.on('ready',LaunchWindow)

function SetAppMenu( template ) {
  const menu = Menu.buildFromTemplate(template)
  Menu.setApplicationMenu(menu)
}
