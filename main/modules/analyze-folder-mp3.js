const ProcessStream = require('./process-stream.js')
const AudioAnalyze = require('./audio-analyze.js')
const CreateMusicCover = require('./create-music-cover.js')
const Find = require('find')
module.exports = (path, {fileAnalyzed, delay} ) => {
  if(path instanceof Array) 
    return new Promise((resolve, reject) => {
      const nestedprocess = new ProcessStream({
        processor: analyzePath,
        processComplete: resolve,
        data: path,
      })
    })
  else return analyzePath(path)
  function analyzePath(path) {
    return new Promise((resolve, reject) => {
      const process = new ProcessStream({
        processor: analyzeAndCreateCover,
        dataProcessed: metadata => fileAnalyzed(metadata, path),
        processComplete: resolve,
        delayEveryProcess: delay
      })
      Find.eachfile(/\.mp3$/, path, filepath => process.addData(filepath))
      .end(() => process.end())
    })
  }
}
function analyzeAndCreateCover( path ) {
  return new Promise((resolve, reject) => {
    AudioAnalyze(path)
    .then(meta => {
      if(meta.picture && meta.picture.length > 0)
        CreateMusicCover(meta)
        .then(ok => resolve(meta))
      else resolve(meta)
    })
  })
}