const MusicMetadata = require('music-metadata')
const CreateMusicCover = require('./create-music-cover.js')
module.exports = analyze
function analyze(path) {
  return new Promise((resolve, reject) => {
    MusicMetadata.parseFile(path)
    .then(metadata => {
      const data = {
        ...metadata.common,
        format: metadata.format.dataformat,
        filepath: path,
        duration: metadata.format.duration,
        unique: toBase64(path + metadata.format.duration)
      }
      resolve(data)
    })
  })
}
function toBase64(str) {
  return Buffer.from(str).toString('base64')
}