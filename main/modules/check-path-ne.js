const Ne = require('./nedb.js')
const Path = require('path')
const isPathInside = require('is-path-inside')
module.exports = checkPath
function checkPath( selectedPath, cb ) {
  return new Promise((resolve, reject) => {
    let okPath = []
    Ne.sync_folder.find({}, 
    (err, docs) => {
      if(docs.length < 1)
        resolve(selectedPath.map(e=>{
          return {path: e}
        }))
      for(let j = 0 ; j < selectedPath.length; j++) {
        let path = selectedPath[j]
        for(let i = 0 ; i < docs.length ; i++ ) {
          let doc = docs[i]
          if(isPathInside(path, doc.path)) {
            set('inside')
            break
          }
          else if(isPathInside(doc.path, path)) {
            set('outside')
            // document path is inside selected path
            break
          }
          else if(isPathEqual(doc.path, path)) {
            set('equal')
            // document path is equal with selected path
            break
          }
          else if(i == docs.length - 1) {
            set()
          }
          function set(type) {
            console.log('ok')
            selectedPath[j] = { path }
            if(type)
              selectedPath[j].error = {type, path: doc.path}
          }
        }
        function callb(name) {
          if(cb && typeof cb[name] == "function") {
            cb[name](path)
          }
        }
      }
      resolve(selectedPath)  
    })
  })
}
function isPathEqual(a, b) {
  const pA = Path.join(a,'/')
  const pB = Path.join(b,'/')
  return pA == pB
}