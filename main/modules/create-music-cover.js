const ProcessStream = require('./process-stream.js')
const Fs = require('fs')
const Sharp = require('sharp')
const Path = require('path')
const CoverPath = Path.join('/home/fhadiel/Node/player/main/musicover')
module.exports =  metadata  => 
  createCoverDirectory( metadata )
  .then(appendCover)


function appendCover({fullPath, picture}) {
  return new Promise((resolve, reject) => {
    const sPro = new ProcessStream({
      processor,
      data: picture,
      end: true,
      processComplete: resolve
    })
  })
  function processor({ format, data }) {
    return new Promise((resolve, reject) => {
      const path = Path.join(fullPath, String(this.processCount))
      Sharp(data)
      .resize(150)
      .toFile(path)
      .then(resolve)
      .catch(reject)
    })
  }
}

function createCoverDirectory( meta ) {
  return new Promise((resolve, reject) => {
    const folderName = `${meta.title} [${meta.unique}]`
    const fullPath = Path.join(CoverPath, folderName)
    Fs.mkdir(fullPath, err => {
      resolve({fullPath, picture: meta.picture})
    })
  })
}