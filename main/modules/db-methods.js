const Ne = require('./nedb.js')

exports.getSyncFolder = () => new Promise((resolve, reject) => 
  Ne.sync_folder.find({}, (err, doc) => {
    if(err) reject(err)
    else resolve(doc.map(e=>e.path))
  })
)

