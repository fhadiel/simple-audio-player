module.exports = [
  {
    label: 'Music',
    submenu: [
      { label: 'Add Music' },
      { label: 'Sync Folder', click: require('./sync-folder.js') }
    ],
  },
  {
    label: 'Playlist',
    submenu: [
      { label: 'My Playlist' },
      { label: 'Add Playlist from Folder' },
      { label: 'Add Playlist from Selected File'}
    ]
  }
]


