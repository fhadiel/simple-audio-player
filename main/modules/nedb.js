const Ne = require('nedb')

const DbPath = {
  sync_folder: './main/databases/sync_folder.db',
  song_metadata: './main/databases/song_metadata.db',
  song_lists: './main/databases/song_lists.db'
}

const Db = {
  sync_folder: new Ne(DbPath.sync_folder),
  song_metadata: new Ne(DbPath.song_metadata),
  song_lists: new Ne(DbPath.song_lists)
}

Db.sync_folder.loadDatabase()
Db.song_metadata.loadDatabase()
// Db.song_lists.loadDatabase()
module.exports = Db
