class streamProcesss {
  constructor({ processor, delayEveryProcess, dataProcessed, processComplete, data, end }) {
    this.listCallback = ['dataProcessed', 'processComplete']
    this.processCount = 0
    this.processor = processor
    this.queue = data || []
    this.fail = []
    this.processing = false
    this.isEnded = end || false
    this.delaying = false
    this.delay = delayEveryProcess
    this.processor = processor
    this.dataProcessed = dataProcessed
    this.processComplete = processComplete
    this.currentData = null

    if(this.queue.length > 0)
      this.executeProcess()
  }

  end( ) {
    this.isEnded = true
  }

  callback(name, ...args) {
    if(this.listCallback.includes(name) && typeof this[name] == "function" ) 
      this[name](...args)
  }
  addData(...data) {
    this.queue.push(...data)
    if(this.processing != true && this.delaying != true)
      this.executeProcess()
  }
  
  executeProcess( ) {
    this.currentData = this.queue.shift()
    this.processing = true 
    this.processCount++
    this.processor(this.currentData)
    .then( result => {
      this.callback('dataProcessed', result, this.currentData)
      this.afterProcess()
    })
    .catch( reject => {
      this.fail.push(reject)
      this.afterProcess()
    })
  }

  afterProcess() {
    if(this.queue.length > 0) {
      if(this.delay > 0) {
        this.delaying = true
        this.processing = false
        setTimeout(() => {
          this.delaying = false
          this.executeProcess()
        }, this.delay)
      }
      else this.executeProcess()
    }
    else {
      if(this.isEnded == true) {
        this.callback('processComplete',{
          total: this.processCount,
          fail: this.fail
        })}
      this.processing = false
    }
  }
}

module.exports = streamProcesss