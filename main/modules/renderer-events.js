const Electron = require('electron')
const AnalyzeFolder = require('./analyze-folder-mp3.js')

Electron.ipcMain.on('scanPath', (eve, path) => {
  const chan = `reply ${path}`
  AnalyzeFolder(path, {
    fileAnalyzed( metadata ) {
      delete metadata.picture
      eve.sender.send(chan, {metadata} )
    } 
  }).then(ok => eve.sender.send(chan, {complete: true}))
})
