const { dialog } = Electron = require('electron')
const Ne = require('./nedb.js')
const AnalyzeFolder = require('./analyze-folder-mp3.js')
const Path = require('path')
const CheckPath = require('./check-path-ne.js')
const CreateMusicCover = require('./create-music-cover')
const ProcessStream = require('./process-stream.js')

module.exports = (menuitem, browserwindow , event) => {
  dialog.showOpenDialog(browserwindow,{
    properties: ['openDirectory','multiSelections']
  }, directorySelected )
  
  function directorySelected(selectedPath, bookmarks) {
    CheckPath(selectedPath)
    .then(path => {
      const successPath = path.filter(e=>!e.error)
      const errPath = path.filter(e=>e.ok != true)
      return successPath
    })
    .then(insertPathToDb)
    // .then(path => {
    //   const createCoverStream = new ProcessStream
    //   AnalyzeFolder( path , {
    //     fileAnalyzed( metadata, syncfolderpath ) {
    //       console.log(metadata)
    //     }
    //   })
    // })

    function insertPathToDb(path) {
      return new Promise((resolve, reject) => 
        Ne.sync_folder.insert(
          path,
          (err, doc) => {
            if(!err) resolve(doc.map(e=>e.path))
          }))
    }

    function NeSongMetadata( metadata ) {
      Ne.song_metadata.update(
        { unique: metadata.unique },
        { metadata },
        { upsert: true },
        ( err, doc ) => {

        }
      )
    }
  }
}


function normalize(path) {
  return Path.join(path, '')
}