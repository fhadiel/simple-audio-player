// using windows.require so the module didn't get bundled by webpack
const Electron = window.require('electron')
import Vue from 'vue' 
import Ne from '../main/modules/nedb.js'
import Vuetify from 'vuetify'
import MainComp from './layouts/Main/index.vue'
import store from './store/index.js'
import router from './router/index.js'
// binding some modules to vue property so it can be used globally
Vue.prototype.$electron = Electron
Vue.prototype.$ne = Ne

Vue.use(Vuetify,{
  primary: "#7E57C2",
  secondary: "#7E57C2",
  accent: "#7E57C2",
  error: "#EC407A",
  warning: "#FDD835",
  info: "#42A5F5",
  success: "#4caf50"
})

window._Vue = new Vue({
  store,
  router,
  render: h => h(MainComp)
}).$mount('#app')
