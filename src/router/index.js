import Vue from 'vue'
import VueRouter from 'vue-router'
import FPlaylist from '../components/playlist/index.vue'
import FMenu from '../components/settings/index.vue'
Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    { path: '/playlist', component: FPlaylist },
    { path: '/settings', component: FMenu }
  ]
})
