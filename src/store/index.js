import Vue from 'vue'
import Vuex from 'vuex'
import settings from './settings.js'
import playlist from './playlists.js'

Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    navOpen: false,
    currentTab: "settings",
  },
  modules: { settings, playlist }
})