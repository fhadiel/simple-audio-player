export default {
  namespaced: true,
  state: {
    playlists: [
      ['Alarbrew'],
      ['Postmarreydian'],
      ['Mouslinghshot'],
      ['Get Anhilated'],
      ['Bum Bum Bum'],
      ['Electronic'],
      ['Mutations']
    ],
  }
}