const Path = window.require('path')
const Electron = window.require('electron')
import Ne from '../../main/modules/nedb.js'
export default {
  namespaced: true,
  state: {
    synchronizedPath: [],
  },
  mutations: {
    setPath(state, path) {
      state.synchronizedPath = path
    },
  },
  actions: {
    getSyncPath({commit}) {
      Ne.sync_folder.find({}, (err, doc) => {
        if(!err) commit('setPath', doc.map(e=>e.path))
      })
    },
    addFolder( ) {
      Electron.remote.dialog
        .showOpenDialog({
          properties: ['openDirectory', 'multiSelections']
        }, dir => {
          dir && dir.length > 0 && pathSelected( dir )
        })
      function pathSelected(path) {
      }
    }
  },
  getters: {

  }
}