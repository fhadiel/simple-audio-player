const AudioAnalyze = require('../main/modules/audio-analyze.js')
const ProcessStream = require('../main/modules/process-stream.js')
const AnalyzeFolder = require('../main/modules/analyze-folder-mp3.js')
const CreateMusicCover = require('../main/modules/create-music-cover.js')
const A ='/media/Secondary/FDocument/Music/NCS/'

const coverStream = new ProcessStream({
  processor: CreateMusicCover,
})
AnalyzeFolder(A, {
  fileAnalyzed( metadata ) {
    coverStream.addData(metadata)
  }
}).then(e => coverStream.end())