const Path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const VueTemplatePlugin = require('vue-loader/lib/plugin.js')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const Webpack = require('webpack')
module.exports = {
  entry: './src/main.js',
  target: 'electron-renderer',
  plugins: [
    new CopyWebpackPlugin([
      {
        from: 'src/assets/*',
        to: '[path][name].[ext]'
      }
    ]),
    new VueTemplatePlugin,
    new Webpack.NamedModulesPlugin,
    new Webpack.HotModuleReplacementPlugin,
    new Webpack.LoaderOptionsPlugin({
      options: {
        stylus: {
          import : ['~vuetify/src/stylus/settings/_colors.styl']
        }
      }
    }),
    new HtmlWebpackPlugin({
      template: './src/index.pug'
    })
  ],
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader',
        include: Path.join(__dirname,'/src')
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.styl(us)?$/,
        use: [ 'vue-style-loader', 'css-loader', 'stylus-loader' ]
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i, 
        loader: "file-loader?name=[path][name].[ext]"
      },
      {
        test: /\.pug$/,
        include: Path.join(__dirname,'/src'),
        oneOf: [
          // this applies to <template lang="pug"> in Vue components
          {
            resourceQuery: /^\?vue/,
            use: ['pug-plain-loader']
          },
          // this applies to pug imports inside JavaScript
          {
            use: ['raw-loader', 'pug-plain-loader']
          }
        ]
      },
    ],
    
  },
  devServer: {
    port: 8080,
    hot: true
  },
  output: {
    filename: 'bundled.js',
    path: Path.join(__dirname, 'dist'),
  },
  mode: 'development'
}